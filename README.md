## iStory iOS app

![iStory App](iStory.gif)

### General

- Application was build upon MVVM architecture
- Separated projects into following main blocks:
	- ViewControllers
	- Views
	- Utillities 
	- Network
	- Model
	- ViewModel   
- Networking is build upon *URLSesion* as a *Webservice* class
- UI is implemented in code (without Storyboard) with usage of Autolayout
- General: DI, defined access level for properties and methods, added **final** for the classes which are not inherited by others  

### Environment

- Xcode 10.1
- Swift 4.2

### To Do

- Separate Views from the navigation logic 
- Improve error handling 
- Add unit tests
