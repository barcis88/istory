//
//  StoryListViewModel.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 13/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

final class StoryListViewModel {
    
    private var stories: [Story] = []
    private var webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }
    
    func fetchStories(completion: @escaping () -> Void) {
        
        self.webService.load(resource: Story.all,
                             queryItems: [URLQueryItem(name: "api-key", value: PlistDataProvider.apiKey())],
                             httpMethod: .GET) { (data, error) in
                                
                                DispatchQueue.main.async {
                                    
                                    guard let stories = data else {
                                        return
                                    }
                                    
                                    self.stories = stories
                                    completion()
                                }
        }
    }
    
    func fetchImage(for indexPath: IndexPath, completion: @escaping (UIImage?) -> Void) {
        
        let story = stories[indexPath.row]
        
        guard let thumbnail = story.thumbnail else {
            completion(nil)
            return
        }
        
        self.webService.loadImage(resource: thumbnail) { (image) in
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
    
    func numberOfRows() -> Int {
        return stories.count
    }
    
    func numerOfSections() -> Int {
        return 1
    }
    
    func story(for indexPath: IndexPath) -> Story {
        return stories[indexPath.row]
    }
}
