//
//  StoryViewModel.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 13/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

enum StorySection: Int, CaseIterable {
    
    case title
    case description
    case author
    case seeMore
    
    var description: String {
        switch self {
        case .title:
            return "Title"
        case .description:
            return "Description"
        case .author:
            return "Author"
        case .seeMore:
            return "See More"
        }
    }
}

final class StoryViewModel {
    
    private var story: Story
    private var webService: WebService
    
    init(story: Story, webService: WebService = WebService()) {
        self.story = story
        self.webService = webService
    }
    
    func subSection() -> String {
        return story.subSection != "" ? story.subSection: story.section
    }
    
    func url() -> String {
        return story.articleUrl
    }
    
    func numberOfSections() -> Int {
        return StorySection.allCases.count
    }
    
    func numberOfRows(in section: Int) -> Int {
        return 1
    }
    
    func titleForHeader(in section: Int) -> String? {
        return StorySection.allCases[section].description
    }
    
    func textForRow(in section: Int) -> String? {
        
        guard let section = StorySection(rawValue: section) else {
            return nil
        }
        
        switch section {
        case .title:
            return story.title
        case .description:
            return story.abstract
        case .author:
            return story.byline
        case .seeMore:
            return story.articleUrl
        }
    }
    
    func fetchImage(completion: @escaping (UIImage?) -> Void) {
        
        guard let medium = story.mediumThreeByTwo210 else {
            completion(nil)
            return
        }
        
        self.webService.loadImage(resource: medium) { (image) in
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
}
