//
//  WebService.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 12/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

enum APIError: Error {
    case NoInternet
    case ParseError
}

enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
}

final class WebService {
    
    func load<A>(resource: Resource<A>, queryItems: [URLQueryItem], httpMethod: HTTPMethod, completion: @escaping (A?, Error?) -> Void) {
        
        let urlComps = NSURLComponents(string: resource.url.absoluteString)
        urlComps?.queryItems = queryItems
        
        guard let URL = urlComps?.url else {
            return
        }
        
        var request = URLRequest(url: URL)
        request.httpMethod = httpMethod.rawValue
        
        URLSession.shared.dataTask(with: request) { (data, _, error) in
            
            guard let data = data else {
                completion(nil, APIError.NoInternet)
                return
            }
            completion(resource.parse(data), nil)
            }.resume()
    }
    
    func loadImage<A>(resource: Resource<A>, completion: @escaping (UIImage?) -> Void) {
        
        URLSession.shared.dataTask(with: resource.url) { data, _, _ in
            
            guard let imageUnWrapped = data else {
                completion(nil)
                return
            }
            let image = UIImage(data: imageUnWrapped)
            completion(image)
            }.resume()
    }
}
