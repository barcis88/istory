//
//  Resource.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 12/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import Foundation

struct Resource<A> {
    let url: URL
    let parse: (Data) -> A?
}

extension Resource {
    init(url: URL, parseJSON: @escaping (Any) -> A?) {
        
        self.url = url
        self.parse = { data in
            let json = try? JSONSerialization.jsonObject(with: data as Data, options: [])
            return json.flatMap(parseJSON)
        }
    }
}
