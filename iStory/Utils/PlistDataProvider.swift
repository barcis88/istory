//
//  PlistDataProvider.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 11/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import Foundation

final class PlistDataProvider {
    static func valueForInfoListKey(_ key: String) -> AnyObject? {
        if let infoDictionary = Bundle.main.infoDictionary {
            return infoDictionary[key] as AnyObject?
        }
        return nil
    }
    
    static func url() -> String {
        guard let key = valueForInfoListKey(Constants.InfoPlistKeys.url) as? String else { return "" }
        return key
    }
    
    static func apiKey() -> String {
        guard let key = valueForInfoListKey(Constants.InfoPlistKeys.apiKey) as? String else { return "" }
        return key
    }
}
