//
//  Constants.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 11/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import Foundation

enum Constants {
    
    enum InfoPlistKeys {
        static let url = "Url"
        static let apiKey = "APIKey"
    }
    
    enum Title {
        static let storyListVC = "Top Stories"
    }
}

