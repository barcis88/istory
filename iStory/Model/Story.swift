//
//  Story.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 11/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

private let url = URL(string: PlistDataProvider.url())

typealias JSONDictionary = [String: Any]

struct Story {
    var title: String
    var abstract: String
    var articleUrl: String
    var byline: String
    var subSection: String
    var section: String
    var multimedia: [Multimedia]
}

extension Story {
    init?(dictionary: JSONDictionary) {
        
        guard let title = dictionary["title"] as? String else { return nil }
        guard let abstract = dictionary["abstract"] as? String else { return nil }
        guard let articleUrl = dictionary["url"] as? String else { return nil }
        guard let byline = dictionary["byline"] as? String else { return nil }
        guard let subSection = dictionary["subsection"] as? String else { return nil }
         guard let section = dictionary["section"] as? String else { return nil }
        guard let multimedia = dictionary["multimedia"] as? [JSONDictionary] else { return nil }
        let unwrappedMultimedia = multimedia.compactMap(Multimedia.init)
        
        self.title = title
        self.abstract = abstract
        self.articleUrl = articleUrl
        self.byline = byline
        self.subSection = subSection
        self.section = section
        self.multimedia = unwrappedMultimedia
    }
}

extension Story {
    static let all = Resource<[Story]>(url: url!, parse: { data in
        let json = try? JSONSerialization.jsonObject(with: data as Data, options: [])
        guard let dictionary = json as? JSONDictionary else { return nil }
        guard let results = dictionary["results"] as? [JSONDictionary] else {
            return nil
        }
        return results.compactMap(Story.init)
    })
}

extension Story {
    
    var thumbnail: Resource<UIImage>? {
                
        guard let unWarppedImageUrlString = multimedia.filter({ $0.format == MultimediaFormat.standardThumbnail }).first?.url,
            let imageUrl = URL(string: unWarppedImageUrlString) else {
            return nil
        }
    
        return Resource<UIImage>(url: imageUrl, parse: { data -> UIImage in
            let image = UIImage(data: data) ?? UIImage()
            return image
        })
    }
    
    var mediumThreeByTwo210: Resource<UIImage>? {
        
        guard let unWarppedImageUrlString = multimedia.filter({ $0.format == MultimediaFormat.mediumThreeByTwo210 }).first?.url,
            let imageUrl = URL(string: unWarppedImageUrlString) else {
                return nil
        }
        
        return Resource<UIImage>(url: imageUrl, parse: { data -> UIImage in
            let image = UIImage(data: data) ?? UIImage()
            return image
        })
    }
}
