//
//  Multimedia.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 12/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import Foundation

enum MultimediaFormat: String {
    case standardThumbnail = "Standard Thumbnail"
    case thumbLarge = "thumbLarge"
    case normal = "Normal"
    case mediumThreeByTwo210 = "mediumThreeByTwo210"
    case superJumbo =  "superJumbo"
}

struct Multimedia {
    var url: String
    var format: MultimediaFormat
}

extension Multimedia {
    init?(dictionary: JSONDictionary) {
        
        guard let url = dictionary["url"] as? String else { return nil }
        guard let format = dictionary["format"] as? String,
            let enumFormat = MultimediaFormat(rawValue: format) else { return nil }
        
        self.url = url
        self.format = enumFormat
    }
}

