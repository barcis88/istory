//
//  ViewController.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 11/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

final class StoryListViewController: UITableViewController {
    
    fileprivate let viewModel: StoryListViewModel
    fileprivate let cellIdentifier = String(describing: StoryTableViewCell.self)
    
    init(viewModel: StoryListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel.fetchStories {
            self.tableView.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupView()
    }
    
    private func setupTableView() {
        tableView.register(StoryTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        navigationItem.title = Constants.Title.storyListVC
    }
}

extension StoryListViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numerOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? StoryTableViewCell else {
            return UITableViewCell()
        }
        
        let story = viewModel.story(for: indexPath)
        cell.setUp(with: story)
        cell.tag = indexPath.row
        
        self.viewModel.fetchImage(for: indexPath) { (image) in
            if cell.tag == indexPath.row {
                cell.thumbnail.image = image
                cell.setNeedsLayout()
            }
        }
        
        return cell
    }
}

extension StoryListViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyViewModel = StoryViewModel(story: viewModel.story(for: indexPath))
        let storyDetailsVC = StoryDetailsViewController(viewModel: storyViewModel)
        self.navigationController?.pushViewController(storyDetailsVC, animated: true)
    }
}
