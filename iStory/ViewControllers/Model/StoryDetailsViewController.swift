//
//  StoryDetailsViewController.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 13/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

final class StoryDetailsViewController: UIViewController {
    
    private let headerImageView = UIImageView()
    private let tableView: UITableView = UITableView()
    private var viewModel: StoryViewModel
    fileprivate let cellIdentifier = String(describing: UITableViewCell.self)
    
    init(viewModel: StoryViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupHeaderImage()
        setupView()
        setupConstraints()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView()
        view.addSubview(tableView)
    }
    
    private func setupHeaderImage() {
        headerImageView.contentMode = .scaleAspectFill
        headerImageView.clipsToBounds = true
        viewModel.fetchImage { (image) in
            self.headerImageView.image = image
        }
        view.addSubview(headerImageView)
    }
    
    private func setupView() {
        view.backgroundColor = .white
        navigationItem.title = viewModel.subSection()
    }
    
    private func setupConstraints() {
        
        headerImageView.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints: [NSLayoutConstraint] = []
        
        constraints.append(headerImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor))
        constraints.append(headerImageView.leftAnchor.constraint(equalTo: view.leftAnchor))
        constraints.append(headerImageView.rightAnchor.constraint(equalTo: view.rightAnchor))
        constraints.append(headerImageView.heightAnchor.constraint(equalToConstant: 200))
        
        constraints.append(tableView.topAnchor.constraint(equalTo: headerImageView.bottomAnchor))
        constraints.append(tableView.leftAnchor.constraint(equalTo: view.leftAnchor))
        constraints.append(tableView.rightAnchor.constraint(equalTo: view.rightAnchor))
        constraints.append(tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor))
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension StoryDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = StorySection(rawValue: indexPath.section) else {
            return
        }
        switch section {
        case .seeMore:
            guard let url = URL(string: viewModel.url()) else { return }
            UIApplication.shared.open(url)
        default:
            break
        }
    }
}

extension StoryDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeader(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  String(describing: UITableViewCell.self), for: indexPath)
        
        guard let section = StorySection(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        switch section {
        case .seeMore:
            cell.textLabel?.textColor = .blue
        default:
            cell.textLabel?.textColor = .black
        }
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.textLabel?.text = viewModel.textForRow(in: indexPath.section)
        cell.selectionStyle = .none
        cell.textLabel?.numberOfLines = 0
        return cell
    }
}
