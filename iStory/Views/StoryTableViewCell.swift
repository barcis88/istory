//
//  StoryTableViewCell.swift
//  iStory
//
//  Created by Bartlomiej Woronin on 13/02/2019.
//  Copyright © 2019 Bartlomiej Woronin. All rights reserved.
//

import UIKit

final class StoryTableViewCell: UITableViewCell {
    
    fileprivate var titleLabel = UILabel()
    fileprivate var authorLabel = UILabel()
    var thumbnail = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUp(with story: Story) {
        titleLabel.text = story.title
        authorLabel.text = story.byline
    }
    
    private func setupView() {
        
        accessoryType = .disclosureIndicator
        
        thumbnail.contentMode = .scaleAspectFit
        thumbnail.layer.cornerRadius = 35
        thumbnail.layer.masksToBounds = true
        
        titleLabel.font = UIFont.systemFont(ofSize: 16)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 2
        
        authorLabel.textColor = .gray
        authorLabel.numberOfLines = 1
        authorLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        addSubview(titleLabel)
        addSubview(authorLabel)
        addSubview(thumbnail)
    }
    
    private func setupConstraints() {
        
        thumbnail.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        authorLabel.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints: [NSLayoutConstraint] = []
        
        constraints.append(titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 16))
        constraints.append(titleLabel.leftAnchor.constraint(equalTo: self.thumbnail.rightAnchor, constant: 8))
        constraints.append(titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -30))
        
        constraints.append(authorLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16))
        constraints.append(authorLabel.leftAnchor.constraint(equalTo: self.thumbnail.rightAnchor, constant: 8))
        constraints.append(authorLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -30))
        
        constraints.append(thumbnail.topAnchor.constraint(equalTo: self.topAnchor, constant: 16))
        constraints.append(thumbnail.widthAnchor.constraint(equalToConstant: 70))
        constraints.append(thumbnail.heightAnchor.constraint(equalToConstant: 70))
        constraints.append(thumbnail.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16))
        constraints.append(thumbnail.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16))
        
        NSLayoutConstraint.activate(constraints)
        
    }
}
